// Function to convert an IP address string to an array of numeric values
function convertIPToArray(IP) {
  // Check if the input IP address string is empty
  if (IP.length === 0) {
    // Log an error message and return null if the IP address is invalid
    console.log("Invalid IP address");
    return null;
  } else {
    // Split the IP address string into an array of strings based on the '.' delimiter
    let StringIPArray = IP.split(".");

    // Convert each string in the array to a number and filter out non-numeric values
    let IPArray = StringIPArray.map((IPBits) => {
      return Number(IPBits);
    }).filter((IPBit) => {
      return IPBit >= 0 && IPBit < 256;
    });

    // Check if the resulting array has exactly 4 numeric values (valid IP address)
    if (IPArray.length === 4) {
      // Return the valid IP address array
      return IPArray;
    } else {
      // Return empty array if the IP address is invalid
      return [];
    }
  }
}

// Export the convertIPToArray function to make it accessible from other files
module.exports = convertIPToArray;
