// Function to convert an array of strings to a single space-separated string
function convertArrayToString(stringArray) {
  // Check if the input stringArray is empty
  if (stringArray.length === 0) {
    // Log a message and return an empty string if the array is empty
    console.log("Empty array");
    return "";
  } else {
    // Use reduce to concatenate the array elements into a single space-separated string
    let convertedString = stringArray.reduce((acc, curr) => {
      return acc + curr + " ";
    }, "");

    // Return the converted string
    return convertedString;
  }
}

// Export the convertArrayToString function to make it accessible from other modules
module.exports = convertArrayToString;
