// Function to extract the month from a complete date string (in the format dd/mm/yyyy)
function getMonth(completeDate) {
  // Check if the input complete date string is empty
  if (completeDate.length === 0) {
    // Log an error message and return null if the date is not provided
    console.log("Enter date");
    return null;
  } else {
    // Split the complete date string into an array based on the '/' delimiter
    let dateArray = completeDate.split("/");

    // Array of month names
    let months = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];

    // Check if the dateArray has exactly 3 elements (day, month, year)
    if (dateArray.length === 3) {
      // Extract day, month, and year from the dateArray and convert them to numbers
      let date = Number(dateArray[0]);
      let month = Number(dateArray[1]);
      let year = Number(dateArray[2]);

      // Check if the extracted values form a valid date
      if (date > 0 && date < 32 && month > 0 && month < 13 && year > 0) {
        // Return the month name based on the month index in the months array
        return months[month - 1];
      } else {
        // Log an error message and return null if the date is invalid
        console.log("Invalid Date Entered");
        return null;
      }
    } else {
      // Log an error message and return null if the date format is invalid
      console.log("Invalid Date Entered");
      return null;
    }
  }
}

// Export the getMonth function to make it accessible from other modules
module.exports = getMonth;
