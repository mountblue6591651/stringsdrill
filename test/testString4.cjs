// Importing the getFullName function from the "string4.cjs" module
const getFullName = require("../string4.cjs");

// Input name object with first_name, middle_name, and last_name properties
let name = { first_name: "JoHN", middle_name: "doe", last_name: "SMith" };

// Calling the getFullName function to concatenate values and form a full name
let fullName = getFullName(name);

// Checking if the concatenation was successful (i.e., if fullName is not falsy)
if (fullName) {
  // Logging the full name to the console if successful
  console.log(fullName);
}
