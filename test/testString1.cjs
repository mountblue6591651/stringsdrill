// Importing the convertToNumber function from the "string1.cjs" file
const convertToNumber = require("../string1.cjs");

// Input string containing a numeric value with a negative sign and non-numeric characters
let str = "-$104.56666";

// Calling the convertToNumber function to convert the string to a numeric value
let convertedString = convertToNumber(str);

// Checking if the conversion was successful (i.e., if convertedString is not falsy)
if (convertedString) {
  // Logging the converted numeric value to the console if successful
  console.log(convertedString);
} else {
  // Logging 0 to the console if the conversion was not successful
  console.log(0);
}
