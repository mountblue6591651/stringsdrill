// Importing the convertIPToArray function from the "string2.cjs" file
const convertIPToArray = require("../string2.cjs");

// Input IP address as a string
let IP = "111.139.161.255";

// Calling the convertIPToArray function to convert the IP string to an array
let IPArray = convertIPToArray(IP);

// Checking if the conversion was successful
if (IPArray) {
  // Logging the converted IP array to the console if successful
  console.log(IPArray);
}
