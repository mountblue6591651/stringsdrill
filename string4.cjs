// Function to convert object containing name to full name 
function getFullName(name) {
  // Check if the name object is empty
  if (Object.keys(name).length === 0) {
    // Log an error message and return null if the name is not provided
    console.log("Name not entered");
    return null;
  } else {
    // Extract values from the name object
    let fullNameArray = Object.values(name);

    // Use reduce to concatenate the values into a single string with a space between them
    let fullName = fullNameArray.reduce((acc, current) => {
      return acc + current + " ";
    }, "");

    // Return the full name
    return fullName;
  }
}

// Export the getFullName function to make it accessible from other modules
module.exports = getFullName;
