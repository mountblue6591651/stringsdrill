// Function to convert a string to a number
function convertToNumber(str) {
  // Check if the input string is empty
  if (str.length === 0) {
    // If the string is empty, return null
    return null;
  } else {
    // Remove the '$' symbol from the string and attempt to convert it to a number
    let convertedString = Number(str.replace("$", ""));

    // Check if the conversion was successful (convertedString is a valid number)
    if (convertedString) {
      // If successful, return the converted numeric value
      return convertedString;
    } else {
      // If the conversion was not successful, return null
      return null;
    }
  }
}

// Export the convertToNumber function to make it accessible from other files
module.exports = convertToNumber;
